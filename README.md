----

## Weather
Simple tool made to check the weather on your local/remote terminal.

## Pictures

![Selection](https://i.imgur.com/tD48R7h.png)

## Installation

	git clone https://github.com/Sorikal/Weather.git
	cd Weather
	python3 weather.py

## Credit

This script was made using the wttr.in website.

## Why was this repo moved?

After Github was sold to Microsoft I no longer felt good about my work being stored on Microsoft servers and every little metadata of my work is kept on their servers.
